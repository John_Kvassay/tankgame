﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float bSpeed;
    public float bTimeLife; // Time that the bullet should live for
    public float bDamage;
    public TankData enemyHit = null;
    public EnemyScoreSender hitBy;
    public int firedby;
    public Transform hitposition;
    public AudioClip hitAudio;
    public bool shootByPlayer;

    // Use this for initialization
    void Start() {
        StartCoroutine(bulletlife());
        
    }

    void Awake()
    { }

    // Deals damage to enemy tank on hit
    public void OnTriggerEnter(Collider other)
    {
        enemyHit = other.gameObject.GetComponent<TankData>();
        enemyHit.currentHealth -= bDamage;

        if (shootByPlayer)
        {
            // System that keeps track of which player shoot what
            // Gets the script on the enemy that keeps tracks of who last shoot it
            hitBy = other.gameObject.GetComponent<EnemyScoreSender>();
            // sets the lastHitBy variable to the index of the tank that fired it
            hitBy.lastHitBy = firedby;
        }
        
        

        hitposition = GetComponent<Transform>();
        AudioSource.PlayClipAtPoint(hitAudio, hitposition.position);

        Destroy(this.gameObject);

        Debug.Log(enemyHit.currentHealth);


    }


    // Update is called once per frame
    void Update () {
        
        
        // Propels the bullet prefab forward
        transform.Translate(Vector3.forward * bSpeed * Time.deltaTime);

	}

    // Destroys the bullet after a certain ammount of time
    IEnumerator bulletlife()
    {
        yield return new WaitForSeconds(bTimeLife);
        Destroy(gameObject);

    }
}
