﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This script holds all of the code needed for tank movement

public class TankMotor : MonoBehaviour
{
    private CharacterController characterController;
    public Transform tf;
    public TankData data;
    
    // Variables For AI
    public float distFromObject = 10; 

    // Use this for initialization
    public void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
        characterController = gameObject.GetComponent<CharacterController>();
        data = GetComponent<TankData>();
       
    }
    public void Move(float speed)
    {
        // Create a vector to hold our speed data 
        Vector3 speedVector;
        speedVector = tf.forward;
        speedVector *= speed;
        characterController.SimpleMove(speedVector);

        

    }

    public void Rotate(float speed)
    {
        Vector3 rotateVector;
        rotateVector = Vector3.up;
        rotateVector *= speed * Time.deltaTime;

        tf.Rotate(rotateVector, Space.Self);
    }

    // Rotates object towards a passed vector

    public bool RotateTowards(Vector3 target)
    {
        Quaternion targetRotation = Quaternion.LookRotation(target);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, data.turnSpeed * Time.deltaTime);
        return true;
    }

    // Pathfinding Functions

    public bool objectInPath()
    {
        // Shoots out a raycast to see if anything is in front of the tank 
        if (Physics.Raycast(transform.position, transform.forward, distFromObject))
        {
            return false;
        }

        return true;

    }

    public void aiPathfindingMove()
    {
        if (objectInPath()) // Checks to see if objectInPath returned false
        {
            Move(data.moveSpeed);
        }
        else
        {
            Rotate(data.turnSpeed);
        }

    }

}
