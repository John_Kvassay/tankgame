﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

    [Header("Input Map Seed Below")]
    public int mapSeed;


    [Header("Grid Number of Columns and Rows")]
    public int rows;
    public int col;

    public float roomWidth = 50.0f;
    public float roomHeight = 50.0f;
    private Room[,] grid;
    public GameObject[] gridPrefabs;


	// Use this for initialization
	void Start () {
        GenerateLevel();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GenerateLevel()
    {

        Random.seed = mapSeed;

        grid = new Room[rows, col];

        // For each grid row
        for (int i = 0; i < rows; i++)
        {
            // For each column in that row
            for (int j = 0; j < col; j++)
            {
                float xPosition = roomWidth * j;
                float zPosition = roomHeight * i;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;
                tempRoomObj.transform.parent = this.transform;
                tempRoomObj.name = "Room_" + j + "," + i;
                Room tempRoom = tempRoomObj.GetComponent<Room>();
                if (i == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (i == rows - 1)
                {
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                // If we are on the first column, open the east door
                if (j == 0)
                {
                    tempRoom.doorEast.SetActive(false);

                }
                if (j == col - 1)
                {
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }
                // Save it to the grid array
                grid[j, i] = tempRoom;

            }
        }




    }


    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[Random.Range(0, gridPrefabs.Length)];
    }


}
