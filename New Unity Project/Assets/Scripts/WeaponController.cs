﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {

    public bool isShooting;
    private float timeBetweenShots;
    public TankData data;
    public Projectile bullet;
    public PlayerIndexAssigner index;
    public AudioData audiodata;
    public PlayerDeathManager player;

    // These are varibles set in the begining by pulling data from TankData
    [Header("Varibles Pulled From TankData")]
    public Transform bulletSpawn;
   
    
   
    // Use this for initialization
   

    private void Awake()
    {
        data = GetComponent<TankData>();
        index = GetComponent<PlayerIndexAssigner>();
        audiodata = GetComponent<AudioData>();
    }

    void Start () { 
		
	}
	
	// Update is called once per frame
	void Update () {

        bulletSpawn = data.firePoint;

        if (isShooting)
        {
            timeBetweenShots -= Time.deltaTime;
            if (timeBetweenShots <= 0)
            {
                timeBetweenShots = data.fireRate;
                Projectile newbullet = Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation) as Projectile;
                // Plays the Fire Sound
                AudioSource.PlayClipAtPoint(audiodata.shoot, bulletSpawn.position);


                // Passes in the values needed for the instantiated bullet
                newbullet.bSpeed = data.bulletSpeed;
                newbullet.bDamage = data.bulletDamage;
                newbullet.bTimeLife = data.bulletLifeTime;

                // System that keeps track of which player shoot what
                // passes the player assigned index from the PlayerIndexAssigner script into the bullet to tell it which player fired it
                if (player)
                {
                    newbullet.firedby = index.playerIndex;
                    newbullet.shootByPlayer = true;
                }
                newbullet.hitAudio = audiodata.bulletHit;

              
            }
            

        }
	}
}
