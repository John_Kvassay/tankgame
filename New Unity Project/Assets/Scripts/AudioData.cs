﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioData : MonoBehaviour {

    public AudioClip shoot;
    //public AudioClip move;
    public AudioClip death;
    public AudioClip bulletHit;
}
