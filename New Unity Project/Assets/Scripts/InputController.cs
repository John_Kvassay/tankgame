﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{ 
    public enum InputScheme { WASD, arrowKeys };
    public InputScheme ControlType; // Naming a variable Input will override the default unity function
    public TankMotor motor;
    public WeaponController weapon;
    
    // Use this for initialization
    
    void Awake()
    {
        motor = GetComponent<TankMotor>();
        weapon = GetComponent<WeaponController>();
    }

    // Update is called once per frame
    void Update()
    {
        InputHandler(); // Becomes a fuction that is allways getting called
    }

    private void InputHandler()
    {
       

        // Movement Controls
        switch (ControlType)
        {
            case InputScheme.WASD:
                WASDController();
                break;
            case InputScheme.arrowKeys:
                ArrowkeyController();
                break;
            default:
                break;
        }
    }

    // Funtion that handles the arrow keys as input
    public void ArrowkeyController()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            motor.Move(motor.data.moveSpeed);
            Debug.Log("w");
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            motor.Move(-motor.data.reverseSpeed);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            motor.Rotate(-motor.data.turnSpeed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            motor.Rotate(motor.data.turnSpeed);
        }
        // Fire Command
        if (Input.GetMouseButtonDown(0))
        {
            weapon.isShooting = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            weapon.isShooting = false;
        }

    }

    // Function that handles the WASD keys as input
    public void WASDController()
    {
        if (Input.GetKey(KeyCode.W))
        {
            motor.Move(motor.data.moveSpeed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            motor.Move(-motor.data.reverseSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            motor.Rotate(motor.data.turnSpeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            motor.Rotate(-motor.data.turnSpeed);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            weapon.isShooting = true;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            weapon.isShooting = false;
        }


    }
}
