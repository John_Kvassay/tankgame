﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {


    [Header("UI refernce variables for player's score counter ")]
    public Text scoreTextp1 = null;
    public int scorep1 = 0;

    public Text scoreTextp2 = null;
    public int scorep2 = 0;



    [Header("UI reference variables for player's life counter")]
    public Text p1Lives = null;
    

    public Text p2Lives = null;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        // Handles score counter for player 1
        scorep1 = GameManager.instanceGM.player1Score;
        string stringScorep1 = scorep1.ToString();
        scoreTextp1.text = stringScorep1;

        // Handles score counter for player 2
        scorep2 = GameManager.instanceGM.player2Score;
        string stringScorep2 = scorep2.ToString();
        scoreTextp2.text = stringScorep2;

        // Handles UI lives counter for player 1
        p1Lives.text = GameManager.instanceGM.player1Lives.ToString();

        // Handles UI lives counter for player 2
        p2Lives.text = GameManager.instanceGM.player2Lives.ToString();

	}
}
