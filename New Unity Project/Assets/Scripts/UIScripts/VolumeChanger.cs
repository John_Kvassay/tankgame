﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class VolumeChanger : MonoBehaviour {

    public Slider musicVolume;
    public AudioSource music;
    public Slider fxVolume;
    public AudioSource buttonFX;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        music.volume = musicVolume.value;
        buttonFX.volume = fxVolume.value;
	}
}
