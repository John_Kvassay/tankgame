﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour {



    public GameObject pickupPrefab;
    public float spawnDelay;
    public float nextSpawnTime;
    public Transform tf;

    public GameObject spawnedPickup;
    
    // Use this for initialization
	void Start () {
        tf = gameObject.GetComponent<Transform>();
        nextSpawnTime = Time.time + spawnDelay;
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.time > nextSpawnTime)
        {
            Instantiate(pickupPrefab, tf.position, Quaternion.identity);
            nextSpawnTime = Time.time + spawnDelay;
        }

        if (spawnedPickup == null)
        {
            if (Time.time > nextSpawnTime)
            {
                spawnedPickup = Instantiate(pickupPrefab, tf.position, Quaternion.identity) as GameObject;
                nextSpawnTime = Time.time + spawnDelay;

            }
            else
            {
                nextSpawnTime = Time.time + spawnDelay;
            }
        }

	}
}
