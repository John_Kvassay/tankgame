﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public Powerup powerup;
    public AudioClip powerUpFeedback;
    public Transform tf;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        // Variable that stores the other objects Power
        PowerUpController powerController = other.GetComponent<PowerUpController>();
        if (powerController != null)
        {
            powerController.AddPowerUp(powerup);

            if (powerUpFeedback != null)
            {
                AudioSource.PlayClipAtPoint(powerUpFeedback,tf.position, 1.0f);
            }

            Destroy(gameObject);
        }
    }

}
