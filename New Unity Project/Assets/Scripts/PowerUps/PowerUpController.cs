﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour {

    // Stores a list of all the powerups on out tank
    public List<Powerup> powerups;

    // Holds a refrence to our TankData
    public TankData data;

	// Use this for initialization
	void Start () {

        data = GetComponent<TankData>();

        // Initializes our list
        powerups = new List<Powerup>();



	}
	
	// Update is called once per frame
	void Update () {

        List<Powerup> expiredPowerups = new List<Powerup>();

        foreach (Powerup power in powerups)
        {
            power.duration -= Time.deltaTime;

            // If time is up, 
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);

            }
        }
        foreach (Powerup power in expiredPowerups)
        {
            power.OnDeactivate(data);
            powerups.Remove(power);
        }
        expiredPowerups.Clear();

	}

    public void AddPowerUp(Powerup powerup)
    {
        powerup.OnActivate(data);

        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
        

    }
}
