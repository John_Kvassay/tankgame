﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour{

    public float speedModifier;
    public float healthModifier;
    public float damageModifier;
    public float fireRateModifier;

    // Controls how long our powerup will last
    public float duration;

    // Determines weither our powerup is permanent
    public bool isPermanent;

    // Adds the modifier to the target TankData when the powerup is active
    public void OnActivate(TankData target)
    {
        target.moveSpeed += speedModifier;
        target.maxHealth += healthModifier;
        target.bulletDamage += damageModifier;
        target.fireRate += fireRateModifier;
    }

    // Subtracts the modifier from the target TankData when the powerup is inactive
    public void OnDeactivate(TankData target)
    {
        target.moveSpeed -= speedModifier;
        target.maxHealth -= healthModifier;
        target.bulletDamage -= damageModifier;
        target.fireRate -= fireRateModifier;

    }

}
