﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{

    public TankData data;
    public float moveSpeed = 3; // This is in meters per second
    public float turnSpeed = 180; // This is in degrees per second

    public float reverseSpeed = 1;

    public float maxHealth = 100;
    public float currentHealth;


    public float playerScore = 0;

    // Bullet info is taken from the TankData and given to the WeaponController
    [Header("Bullet Variables")]
    public float bulletSpeed = 1;
    public float fireRate;
    public Transform firePoint;
    public float bulletDamage = 0;
    public float bulletLifeTime = 3;

    void Start()
    {
        currentHealth = maxHealth;
    }

    void Update()
    {
       
    }

}
