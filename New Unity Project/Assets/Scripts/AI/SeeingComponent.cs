﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeingComponent : MonoBehaviour {

    public float seeingDistance;
    public bool isSeeing;

    public GameObject playerTarget;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 vectorToTarget = playerTarget.transform.position - transform.position;
        RaycastHit hit;


        if (Physics.Raycast(transform.position, vectorToTarget, out hit, seeingDistance))
        {
            if (hit.collider != null)
            {
                isSeeing = true;
                Debug.Log("No Enemy");
            }
            else
            {
                Debug.Log("No Enemy");

            }
            
        }





	}
}
