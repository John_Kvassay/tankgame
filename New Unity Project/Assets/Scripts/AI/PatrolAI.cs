﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolAI : MonoBehaviour {

    // Tank Movement Script Refrenences
    public TankMotor motor;
    public TankData data;

    public float withinPointRange = 1;
    public List<Transform> patrolPoints;
    private Transform location;
    private int currentPatrolPoint = 0;
    public int numOfPatrolPoint; // Keeps track of the number of patrol points


    // Use this for initialization
    void Awake() {

        // Gets the TankMotor and TankData scripts
        motor = gameObject.GetComponent<TankMotor>();
        data = gameObject.GetComponent<TankData>();

        // Gets the current transform for the gameobject
        location = gameObject.GetComponent<Transform>();

        numOfPatrolPoint = patrolPoints.Count;
    }

    // Update is called once per frame
    // Used FixedUpdate in order to ensure it called at consistant intervals
    void FixedUpdate() {

        // Checks to see if the tank has reached the end of the waypoint list. Will reset it back to the begining of the list so it can loop back through tthe patrol points
        if (currentPatrolPoint >= numOfPatrolPoint)
        {
            currentPatrolPoint = 0;
        }

        // gets the distance from the next Waypoint
        float distFromPoint = Vector3.Distance(patrolPoints[currentPatrolPoint].position, location.position);
        // gets the distance from the player
        float distFromPlayer = Vector3.Distance(GameManager.instanceGM.player.transform.position, location.position);
        
        // Gets the Next Waypoint and gets the direction
        Vector3 targetWaypoint = new Vector3(patrolPoints[currentPatrolPoint].position.x, transform.position.y, patrolPoints[currentPatrolPoint].position.z);
        Vector3 dirNextPatrolPoint = targetWaypoint - transform.position;

        // Moves the tank to the next waypoint
        motor.RotateTowards(dirNextPatrolPoint); // Rotates the tank towards the next waypoint
        motor.Move(data.moveSpeed); // moves the Tank

        // Checks to see if the distance from the patrol point is less than or equal to the withinPointRange. It it is, it will update the currentPatrolPoint to the nest Point in the list
        // Will then update the current patrol point
        if (distFromPoint <= withinPointRange)
        {
            currentPatrolPoint += 1; 
        }


    }

    
   

}
