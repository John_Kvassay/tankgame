﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPersonality : MonoBehaviour {

    public enum aiStates
    {
        Patrol,
        TurretIdle,
        SniperFlee,
        Shoot,
        Chase,
    }

    public aiStates currentAIState;

    // Tank Movement Script Refrenences
    
    public TankMotor motor;
    public TankData data;
    public WeaponController weapon;

    public float playerEngagementRange = 7;

    // Patrol Waypoint Movement Variables
    [Header("Patrol Movement Variables")]
    public float withinPointRange = 1;
    public List<Transform> patrolPoints;
    private Transform location;
    private int currentPatrolPoint = 0;
    public int numOfPatrolPoint; // Keeps track of the number of patrol points
    
    public float distFromPlayer;
    public Transform playerLocation;

    // Flee Movement Variables
    public float fleeDistance = 1.0f;


    //Pathfinding Variables
    [Header("Pathfinging Variables")]
    public Transform Target;
    public Transform tf;
    public int pathfindingState = 0; // Keeps track of which state of pathfinding the AI is in
    public float avoidenceTime = 2.0f;
    public float exitTime;

    //Hearing and Seeing Variables
    public float fieldOfViewAngle;
    public float viewRange = 110;



    // Use this for initialization
    void Start()
    {

      
    }

    void Awake()
    {
        
        // Gets the TankMotor and TankData scripts
        motor = gameObject.GetComponent<TankMotor>();
        data = gameObject.GetComponent<TankData>();
        weapon = gameObject.GetComponent<WeaponController>();
        
       
    }


    // Update is called once per frame
    void Update() {


        // gets the distance from the player
        distFromPlayer = Vector3.Distance(GameManager.instanceGM.player.transform.position, transform.position);
        playerLocation = GameManager.instanceGM.player.transform;


        // Gets the TankMotor and TankData scripts
        motor = gameObject.GetComponent<TankMotor>();
        data = gameObject.GetComponent<TankData>();
        weapon = gameObject.GetComponent<WeaponController>();
        tf = gameObject.GetComponent<Transform>();


        switch (currentAIState)
        {
            case aiStates.Patrol:   
                break;
            case aiStates.Shoot:
                ShootState();
                break;
            case aiStates.TurretIdle:
                turretIdleState();
                break;
            case aiStates.Chase:
                break;
            case aiStates.SniperFlee:
                break;

        }



        




        




        // Pathfinding Checks
        if (currentAIState == aiStates.Chase)
        {
            if (pathfindingState != 0)
            {
                DoAvoidence();
            }
            else
            {
                ChaseAI();
            }
        }

        if (currentAIState == aiStates.Patrol)
        {
            if (pathfindingState != 0)
            {
                DoAvoidence();
            }
            else
            {
                PatrolState();
            }

        }

        if (currentAIState == aiStates.SniperFlee)
        {
            if (pathfindingState != 0)
            {
                DoAvoidence();

            }
            else
            {
                Flee();
            }


        }
        
    }



    // Pathfinding


    // Checks to see if anything is in th way
    public bool CanMove(float speed)
    {
        RaycastHit hit;
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            return false;
            
        }
        else
        {
            return true;
        }


    }

    // Runs the avoidence
    void DoAvoidence()
    {
        if (pathfindingState == 1)
        {
            motor.Rotate(data.turnSpeed);
            if (CanMove(data.moveSpeed))
            {
                pathfindingState = 2;

                exitTime = avoidenceTime;
            }

        }
        else if(pathfindingState == 2)
        {
            if (CanMove(data.moveSpeed))
            {
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);


                if (exitTime <= 0)
                {
                    pathfindingState = 0;

                }

            }
            else
            {
                pathfindingState = 1;
            }
        }

    }




    void Flee()
    {
        Vector3 vectortoTarget = GameManager.instanceGM.player.transform.position - tf.position;
        Vector3 vectorAwayFromTarget = -1 * vectortoTarget;
        vectorAwayFromTarget.Normalize();
        vectorAwayFromTarget *= fleeDistance;
        Vector3 fleeposition = vectorAwayFromTarget + tf.position;
        motor.RotateTowards(fleeposition);
        motor.Move(data.moveSpeed);

    }


    void ChaseAI()
    {
        motor.RotateTowards(playerLocation.position);
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
            
        }
        else
        {
            pathfindingState = 1;
            
        }
    }






    void PatrolState()
    {

        // Gets the current transform for the gameobject
        location = gameObject.GetComponent<Transform>();


       
        numOfPatrolPoint = patrolPoints.Count;

        // Checks to see if the tank has reached the end of the waypoint list. Will reset it back to the begining of the list so it can loop back through tthe patrol points
        if (currentPatrolPoint >= numOfPatrolPoint)
        {
            currentPatrolPoint = 0;
        }

        // gets the distance from the next Waypoint
        float distFromPoint = Vector3.Distance(patrolPoints[currentPatrolPoint].position, location.position);
        

        // Gets the Next Waypoint and gets the direction
        Vector3 targetWaypoint = new Vector3(patrolPoints[currentPatrolPoint].position.x, transform.position.y, patrolPoints[currentPatrolPoint].position.z);
        Vector3 dirNextPatrolPoint = targetWaypoint - transform.position;

        // Moves the tank to the next waypoint
        motor.RotateTowards(dirNextPatrolPoint); // Rotates the tank towards the next waypoint
        motor.Move(data.moveSpeed); // moves the Tank

        // Checks to see if the distance from the patrol point is less than or equal to the withinPointRange. It it is, it will update the currentPatrolPoint to the nest Point in the list
        // Will then update the current patrol point
        if (distFromPoint <= withinPointRange)
        {
            currentPatrolPoint += 1;
        }

        if (distFromPlayer <= playerEngagementRange)
        {
            currentAIState = aiStates.Shoot;
        }
        else
        {
            Debug.Log("Null");
        }

    }

    void ShootState()
    {

        motor.RotateTowards(GameManager.instanceGM.player.transform.position);
        weapon.isShooting = true;
        

    }


    void turretIdleState()
    {
        motor.Rotate(data.turnSpeed);
       

        if (distFromPlayer <= playerEngagementRange)
        {
            currentAIState = aiStates.Shoot;
        }
        

    }

    


}
