﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerDeathManager : MonoBehaviour {

    public Transform spawnpoint;
    public TankData data;
    public PlayerIndexAssigner index;

    // Use this for initialization
    void Start () {

        spawnpoint = GameManager.instanceGM.spawnPoint.transform;
        data = GetComponent<TankData>();
        index = GetComponent<PlayerIndexAssigner>();
	}
	
	// Update is called once per frame
	void Update () {

        if (data.currentHealth <= 0)
        {
            if (index.playerIndex == 0)
            {
                transform.position = spawnpoint.position;
                GameManager.instanceGM.player1Lives -= 1;
                data.currentHealth = data.maxHealth;
            }
            if (index.playerIndex == 1)
            {
                transform.position = spawnpoint.position;
                GameManager.instanceGM.player2Lives -= 1;
                data.currentHealth = data.maxHealth;
            }



              
        }



	}
}
