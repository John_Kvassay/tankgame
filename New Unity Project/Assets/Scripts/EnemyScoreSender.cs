﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScoreSender : MonoBehaviour {


    public TankData data;
    public int lastHitBy;
    public int scoreValue;

	// Use this for initialization
	void Start () {

        data = GetComponent<TankData>();


	}
	
	// Update is called once per frame
	void Update () {
        if (data.currentHealth <= 0)
        {
            if (lastHitBy == 0)
            {
                GameManager.instanceGM.player1Score = GameManager.instanceGM.player1Score + scoreValue;
                Destroy(gameObject);
            }
            if (lastHitBy == 1)
            {
                GameManager.instanceGM.player2Score = GameManager.instanceGM.player2Score + scoreValue;
                Destroy(gameObject);
            }


            // Subtracts the tanks from the total number of enemy tanks remaining
            GameManager.instanceGM.enemyTanksRemaining -= 1;

            
        }


	}
}
