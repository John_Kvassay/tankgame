﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public SceneLoader sceneloader;

    public static GameManager instanceGM; // Holds the GameManager instance

        
        public float waitToCount = 2;

        public Transform spawnPoint;        

        public GameObject player;
        public GameObject player2;

        public int player1Lives = 3;
        public int player1Score;

        public int player2Lives = 3;
        public int player2Score;

        public List<GameObject> enemyTanks;
        
        // enemyTanksRemaining set to 1 at the begining to stop the win condition from triggering
        public int enemyTanksRemaining;

    void Awake()
    {
        // enemyTanksRemaining set to 1 at the begining to stop the win condition from triggering
        enemyTanksRemaining = 1;

        // Creates the GameManager to hold all necessary values
        if (instanceGM == null)
        {
            instanceGM = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

    }

    void Start()
    {
        StartCoroutine(waitToCalculateEnemies());
    }

    void Update()
    {

        // Checks for win/loss conditions
        if (player1Lives <= 0)
        {
            sceneloader.loadScene("LossScreen");
            player1Lives = 3;
        }

        if (player2Lives <= 0)
        {
            sceneloader.loadScene("LossScreen");
            player2Lives = 3;
        }

        if (enemyTanksRemaining <= 0)
        {
            sceneloader.loadScene("WinScreen");
            enemyTanksRemaining = 1;

        }




    }

    // Tells the game to wait to count the number of enemies in the list
    IEnumerator waitToCalculateEnemies()
    {
        yield return new WaitForSeconds(waitToCount);
        enemyTanksRemaining = enemyTanks.Count;
    }

}
